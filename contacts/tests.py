from django.core.urlresolvers import resolve
from django.test import TestCase

from contacts.models import Contact
from contacts.views import contact_list

# Create your tests here.

class ContactListViewTests(TestCase):

    def test_that_there_is_a_view_at_root_url(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_that_root_url_calls_contact_list_view(self):
        found = resolve("/")
        self.assertEqual(found.func, contact_list)

class ContactListModelTests(TestCase):

    def setUp(self):
        self.c1 = Contact.objects.create(name="Brock Curry", age=29)

    def test_that_there_is_a_contacts_model(self):
        contact = Contact.objects.get(name="Brock Curry")
        self.assertEqual(self.c1.age, 29)
